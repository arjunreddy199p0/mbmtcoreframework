Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '12.0'
s.name = "MBMTCoreFramework"
s.summary = "MBMTCoreFramework is my test framework"
s.requires_arc = true

# 2
s.version = "0.1.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "arjunreddy199p0" => "mallikarjun.it741@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://bitbucket.org/arjunreddy199p0/mbmtcoreframework/src/Develop/"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://arjunreddy199p0@bitbucket.org/arjunreddy199p0/mbmtcoreframework.git", 
             :tag => "#{s.version}" }

# 7
# s.framework = "UIKit"
s.dependency 'AFNetworking', '~> 4.0'
s.dependency 'Firebase/InAppMessaging'

# 8
s.source_files = "MBMTCoreFramework/**/*.{h,m,swift}"

# 9
# s.resources = "MBMTCoreFramework/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
# s.swift_version = "4.2"

end
